# Cisco IOS XR Software Upgrade

## Table of Contents

* [Overview](#overview)
* [Features](#features)
* [Requirements](#requirements)
* [Known Limitations](#known-limitations)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Components](#components)
  * [Workflows](#workflows)
  * [Command Templates](#command-templates)
* [Stubbed Tasks](#stubbed-tasks)
* [Metrics](#metrics)
  * [Command Template Results](#command-template-results)
  * [Template Diffs](#template-diffs)
* [Test Environment](#test-environment)
* [Additional Information](#additional-information)

## Overview

The **Cisco IOS XR Upgrade** pre-built consists of several workflows and command templates meant to serve as a template for performing a software upgrade on Cisco routers running IOS XR software.

This pre-built was designed and tested using the upgrade guidelines for the [Cisco XRv 9000](https://www.cisco.com/c/en/us/support/docs/routers/ios-xrv-9000-router/212578-xrv-9000-general-upgrade-guideline.html). The upgrade steps are similar to the process for the [NCS 5500](https://www.cisco.com/c/en/us/td/docs/iosxr/ncs5500/system-setup/60x/b-ncs5500-system-setup-guide-60x/b-system-setup-ncs5k_chapter_0110.html) series, [NCS 6000](https://www.cisco.com/c/en/us/td/docs/routers/ncs6000/software/install/guide/b-sysadmin-ig-ncs6k-61x/b-sysadmin-ig-ncs6k-61x_chapter_0111.html) series, [ASR 9000](https://www.cisco.com/c/en/us/td/docs/routers/asr9000/software/asr9k-r6-4/system-management/configuration/guide/b-system-management-cg-asr9000-64x/b-system-management-cg-asr9000-64x_chapter_01011.html) series and [CRS](https://www.cisco.com/c/en/us/td/docs/routers/crs/software/crs_r4-3/system_management/configuration/guide/b_sysman_cg43crs/b_sysman_cg43crs_chapter_010.html) routers. Consequently, this pre-built should upgrade those devices with little to no modifications. As a best practice, we recommend that you use a Command Template for your specific device and software version, as well as review the documentation for this pre-built, to determine if modifications are necessary.

Of note, since this pre-built is a template for performing an IOS XR upgrade, it is not comprehensive. The included pre and post checks should be customized to suit the needs of the target device. Additionally, the implementation of routing network traffic is the responsibility of the user to provide. The pre-built provides an indication of where a failover and restore should take place, but assumes no responsibility for these tasks.

### Main Workflow

Pictured below is the main workflow for this pre-built, followed by a numbered breakdown of each activity in the automation.

<table><tr><td>
  <img src="./images/workflow.png" alt="install" width="800px">
</td></tr></table>

1. Get device details to confirm device is running IOS XR and confirm that PIE is available to the device.
1. Check to make sure that any provided SMUs are available to the device. If running in [verbose mode](#verbose-mode-and-zero-touch), the user will be given a soft warning for any SMUs not found and the upgrade process will continue.
1. Pre and post upgrade checks are run. If running in [verbose mode](#verbose-mode-and-zero-touch), command template results will be shown. Refer to the [Command Templates](#command-templates) section in order to customize these checks for your needs.
1. Failover and restore are provided as a stub task. Refer to the [Stubbed Tasks](#stubbed-tasks) section for more information on implementing custom logic for network failover and restore.
1. The upgrade procedure is divided into separate workflows, which consist of running a series of asynchronous installation commands via Command Templates. This process must be performed separately for PIEs and SMUs. Refer to the [Perform Upgrade](#perform-upgrade) and [Commit Install](#commit-install) sections for more information.

## Features

Notable features for this pre-built are listed below:

* Performs full upgrade and downgrade of Cisco IOS XR software.
* Has a modular design, with each phase of the upgrade process broken out into separate workflows to allow greater customization and flexibility.
* Allows the user to provide location and image filenames for PIEs and (optionally) SMUs.
* Verifies existence of PIE and SMU images before upgrade.
* Provides indication in workflow where failover and restore of network traffic should occur.
* Commits installation and cleans installation repository in between upgrading PIEs and SMUs.
* Provides MOP templates for all stages of the upgrade and downgrade process.
  * Pre and post upgrade check command templates can be customized on a per use case basis.
  * Command templates are included for each phase of the installation process, including management of the installation repository.
* Shows template diffs between pre and post checks.
* Admin option will execute all commands against the device in admin mode.
* Verbose option displays command template results and all error logging.
* Zero touch option executes automation end-to-end without any manual tasks (with the exception of error handling).

## Requirements

Pre-requisites to use the IOS XR Upgrade pre-built include:

* Itential Automation Platform
  * `^2022.1`

## Known Limitations

At the time of this writing, this pre-built is currently only capable of upgrading Ansible devices. Support for NSO will be added in a future update.

## How to Install

The IOS XR Upgrade pre-built can be installed from within App - Admin Essentials. Simply search for `cisco-ios-xr-upgrade` and click the **Install** button as shown below.

<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>

After the notification displays to inform you that the pre-built has been installed, you should be able to navigate to **Operations Manager** and verify that the `IOS XR Upgrade` Operations Manager item has been installed.

## How to Run

As a starting point, this pre-built assumes the installation images necessary to perform the software upgrade are already available to the device - whether over TFTP from a server on the network or on local storage media. If assistance is needed with this phase of the process, refer to the [File Transfer pre-built](https://gitlab.com/itentialopensource/pre-built-automations/file-transfer), which is also available from the **Itential Open Source** repository.

Before running this pre-built, carefully review the documentation for your router as well as the [Components](#components) section below in order to customize the workflows and command templates as necessary.

The upgrade process can be started through **Operations Manager**. First, select the target device from the drop down list of available devices. Next, supply the location of the PIE (Package Installation Envelope) as well as the name of the image file.

<table><tr><td>
  <img src="./images/addpie.png" alt="add PIE image" width="600px">
</td></tr></table>

> **Note:** Check the `Admin` box to run all installation commands against the device in `admin` mode.

Additionally, if you want to simultaneously apply any SMUs (Software Maintenance Upgrades) you may supply the location and file names as shown below. Simply press the `+` button to add filenames to the list.

<table><tr><td>
  <img src="./images/addsmu.png" alt="add SMU image" width="600px">
</td></tr></table>

### Verbose Mode and Zero Touch

This pre-built supports options for limiting or expanding the necessary amount of manual interaction while running:

* `Verbose Mode`: Enable verbose mode to view additional error logging and debugging information throughout the upgrade process, including MOP template results for pre and post checks.
* `Zero Touch`: Enable zero touch to perform the entire upgrade process without any interaction necessary. You should still monitor the state and progress of the device throughout the upgrade. Likewise, any errors that are encountered will still need your attention in order for them to be handled.

When you are ready to initiate the upgrade process, press the `RUN` button to begin.

<table><tr><td>
  <img src="./images/verboseandzt.png" alt="add SMU image" width="600px">
</td></tr></table>

## Components

This pre-built is comprised of a set of modular components intended to simplify the process of customizing the IOS XR upgrade process to suit your device and environment. **At the very least, the pre and post checks command templates should be modified to ensure success.**

### Workflows

### Pre Post Checks

This workflow runs pre and post checks on the device. If [verbose mode](#verbose-mode-and-zero-touch) is enabled, command template results will be shown for the checks as well as the currently active install.

<table><tr><td>
  <img src="./images/prepost.png" alt="add SMU image" width="800px">
</td></tr></table>

#### Perform Upgrade

This workflow performs four (4) steps necessary for installation:

1. `install add source`: This command adds installation images to the installation repository.
1. `install verify packages`: This command verifies the integrity of packages that have been added to the installation repository.
1. `install prepare`: This command prepares the packages that were added to the repository for activation.
1. `install activate`: This command will activate the installation and reload the device. After the device comes back online, the upgrade can be reverted by reloading the device. Otherwise, committing the installation will make the upgrade permanent.

<table><tr><td>
  <img src="./images/performupgrade.png" alt="add SMU image" width="800px">
</td></tr></table>

#### Async Install Operation

This workflow can be used in conjunction with any installation command that runs asynchronously. The workflow will periodically ping the device with the command `show install request` until the device reports that no installation operation is in progress.

<table><tr><td>
  <img src="./images/asyncop.png" alt="add SMU image" width="800px">
</td></tr></table>

If an error is detected, the workflow will collect log information from the device to display to the user, as demonstrated below.

<table><tr><td>
  <img src="./images/errorlog.png" alt="add SMU image" width="800px">
</td></tr></table>

#### Check Connection

This workflow is based on the [Device Connection Health Check](https://gitlab.com/itentialopensource/pre-built-automations/device-connection-health-check) pre-built. The workflow will periodically ping the device until it comes back online after the upgrade and the connection is stable.

<table><tr><td>
  <img src="./images/checkconnection.png" alt="add SMU image" width="800px">
</td></tr></table>

### Commit Install

This workflow also makes use of the [Async Install Operation](#async-install-operation) workflow to perform the following operations:

1. `install commit`: This command commits the upgrade, making it permanent.
1. `install remove inactive`: This command cleans the installation repository by removing all inactive and non-committed packages from the boot device.

<table><tr><td>
  <img src="./images/commitinstall.png" alt="add SMU image" width="800px">
</td></tr></table>

> **Note:** The user will be prompted before the commit (unless [zero touch](#verbose-mode-and-zero-touch) is enabled) to execute a rollback. Here the rollback is provided as a stub task, and it is up to the user to implement the logic.

### Command Templates

This pre-built contains several command templates, which may all be customized. Most of the command templates provided should work out of the box. However, please review all provided command templates and documentation for your device before use.

#### Pre and Post Checks

The provided command template contains a few commonly used pre and post upgrade checks. This is only intended to be a starting point and is not comprehensive.

<table><tr><td>
  <img src="./images/preposttemplate.png" alt="add SMU image" width="800px">
</td></tr></table>

> **Note:** This command template is intended to be customized per use case. Please consult the MOP template for your device.

## Stubbed Tasks

Several tasks in this pre-built have been provided as stub tasks. These tasks represent use case specific logic and are intended to be replaced. For ideal results and best practices, it is recommended that the logic for these jobs is built as separate workflows, and the stub tasks are replaced with the `childJob` task to incorporate the custom logic.

### Failover

Network traffic failover has been provided in the main workflow as a stub task. This task indicates where the workflow network traffic should be re-routed in order to perform the upgrade process.

<table><tr><td>
  <img src="./images/failover.png" alt="add SMU image" width="800px">
</td></tr></table>

### Restore

Similar to the failover, this stub task indicates where in the workflow network traffic should be restored to the device.

<table><tr><td>
  <img src="./images/restore.png" alt="add SMU image" width="800px">
</td></tr></table>

### Rollback

Due to the fact that the rollback procedure for IOS XR varies from device to device, the logic for the rollback has been provided here as a stub task. Please refer to the [Commit Install](#commit-install) workflow for more information on where rollback is intended to be implemented.

## Metrics

### Command Template Results

If [verbose mode](#verbose-mode-and-zero-touch) is enabled, you will be presented with command template results for pre and post checks and the active installation on the device. See the images below for reference.

#### Pre and Post Checks

<table><tr><td>
  <img src="./images/prechecks.png" alt="add SMU image" width="800px">
</td></tr></table>

#### Active Install

<table><tr><td>
  <img src="./images/activeinstall.png" alt="add SMU image" width="800px">
</td></tr></table>

### Template Diffs

Unless [zero touch](#verbose-mode-and-zero-touch) is enabled, the user will be provided with diffs of the pre and post checks after the upgrade process has finished. This is an opportunity to review changes to the device made by the upgrade process.

<table><tr><td>
  <img src="./images/fulldiff.png" alt="add SMU image" width="400px">
</td></tr></table>

## Test Environment

This pre-built was tested in the Itential lab with the Cisco XRv 9000, a virtual router running on VMware. The upgrade and downgrade procedure was tested using the official IOS XR PIE images available from Cisco for versions `6.3.3` and `6.5.3`, as well as all available SMUs for each mentioned release.

All testing was conducted using version `2020.2.1` of the Itential Automation Platform.

## Additional Information

Please use your Itential Customer Success account if support is needed when using this pre-built.
