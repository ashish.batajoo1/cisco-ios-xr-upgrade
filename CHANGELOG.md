
## 0.2.7 [06-27-2022]

* patch/DSUP-1361

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!36

---

## 0.2.6 [05-10-2022]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!35

---

## 0.2.5 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!34

---

## 0.2.4 [03-04-2021]

* patch/DSUP-886-fix-JST-path-master

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!33

---

## 0.2.3 [03-02-2021]

* Implementing isAlive retry

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!30

---

## 0.2.2 [03-02-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!26

---

## 0.2.1 [09-24-2020]

* Update bundles/json_forms/IOS XR Upgrade.json,...

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!24

---

## 0.2.0 [06-18-2020]

* [minor/LB-404] Switch absolute path to relative path

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!22

---

## 0.1.0 [06-17-2020]

* Update manifest to reflect gitlab name

See merge request itentialopensource/pre-built-automations/Cisco-IOS-XR-Upgrade!21

---

## 0.0.11 [05-01-2020]

* patch/README.md - Update

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!20

---

## 0.0.10 [04-17-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!19

---

## 0.0.9 [04-17-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!19

---

## 0.0.8 [04-15-2020]

* Updated IAPDependencies and removed app-artifacts as dependency

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!19

---

## 0.0.7 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!17

---

## 0.0.6 [03-24-2020]

* Migrate files for 2019.3

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!16

---

## 0.0.5 [03-10-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!13

---

## 0.0.4 [02-26-2020]

* Add artifacts keyword

See merge request itentialopensource/pre-built-automations/cisco-ios-xr-upgrade!12

---

## 0.0.3 [02-18-2020]

* Create README

See merge request itentialopensource/pre-built-automations/staging/cisco-ios-xr-upgrade!1

---

## 0.0.2 [02-18-2020]

* Create README

See merge request itentialopensource/pre-built-automations/staging/cisco-ios-xr-upgrade!1

---
